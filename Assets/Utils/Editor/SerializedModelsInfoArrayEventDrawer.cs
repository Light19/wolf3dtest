﻿using UnityEditor;

namespace Wolf3DTest
{
    [CustomPropertyDrawer(typeof(SerializedModelsInfoArrayEvent))]
    public class SerializedModelsInfoArrayEventDrawer : SerializedGenericEventDrawer<IModelsInfoProvider[]>
    {
    }
}