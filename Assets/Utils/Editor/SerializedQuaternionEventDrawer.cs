﻿using UnityEditor;
using UnityEngine;

namespace Wolf3DTest
{
    [CustomPropertyDrawer(typeof(SerializedQuaternionEvent))]
    public class SerializedQuaternionEventDrawer : SerializedGenericEventDrawer<Quaternion>
    {
    }
}