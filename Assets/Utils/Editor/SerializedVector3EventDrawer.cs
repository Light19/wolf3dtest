﻿using UnityEditor;
using UnityEngine;

namespace Wolf3DTest
{
    [CustomPropertyDrawer(typeof(SerializedVector3Event))]
    public class SerializedVector3EventDrawer : SerializedGenericEventDrawer<Vector3>
    {
    }
}