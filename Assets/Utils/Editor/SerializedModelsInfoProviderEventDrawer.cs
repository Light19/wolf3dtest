﻿using UnityEditor;

namespace Wolf3DTest
{
    [CustomPropertyDrawer(typeof(SerializedModelsInfoProviderEvent))]
    public class SerializedModelsInfoProviderEventDrawer : SerializedGenericEventDrawer<IModelsInfoProvider>
    {
    }
}