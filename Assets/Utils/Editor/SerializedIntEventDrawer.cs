﻿using UnityEditor;
using UnityEngine;

namespace Wolf3DTest
{
    [CustomPropertyDrawer(typeof(SerializedIntEvent))]
    public class SerializedIntEventDrawer : SerializedGenericEventDrawer<int>
    {
    }
}