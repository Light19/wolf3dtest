﻿using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Wolf3DTest
{
    [CustomPropertyDrawer(typeof(SerializedGenericEvent<>))]
    public class SerializedGenericEventDrawer<T> : PropertyDrawer
    {
        private const float _spaceBetweenLines = 4;
        private const float _spaceBetweenProperties = 4;
        private float _lineHeight { get { return (float)typeof(EditorGUI).GetField("kSingleLineHeight", BindingFlags.Static | BindingFlags.NonPublic).GetValue(null); } }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            var targets = property.FindPropertyRelative("_permanentTargets");
            var methods = property.FindPropertyRelative("_permanentMethods");

            var singleLineHeightCached = _lineHeight;

            var labelRect = new Rect(position.x, position.y + _spaceBetweenProperties, position.width, singleLineHeightCached + _spaceBetweenLines);
            var targetsRect = new Rect(position.x, labelRect.y + labelRect.height, position.width, (singleLineHeightCached + _spaceBetweenLines) * targets.arraySize);
            var addNewTargetButtonRect = new Rect(position.x, targetsRect.y + targetsRect.height, position.width, singleLineHeightCached);

            EditorGUI.LabelField(labelRect, $"{label.text} Event:");

            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            for (int i = 0; i < targets.arraySize; i++)
            {
                drawDelegateTargets(targetsRect, singleLineHeightCached, i, targets, methods);
            }
            if (GUI.Button(addNewTargetButtonRect, "Add new listener"))
            {
                targets.InsertArrayElementAtIndex(targets.arraySize);
                targets.GetArrayElementAtIndex(targets.arraySize - 1).objectReferenceValue = null;
                methods.InsertArrayElementAtIndex(methods.arraySize);
                methods.GetArrayElementAtIndex(methods.arraySize - 1).stringValue = null;
                targets.serializedObject.ApplyModifiedProperties();
                methods.serializedObject.ApplyModifiedProperties();
            }

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }

        private void drawDelegateTargets(Rect rect, float lineHeight, int index, SerializedProperty targets, SerializedProperty methods)
        {
            if (targets.arraySize < index + 1 || methods.arraySize < index + 1)
                return;

            var y = rect.y + lineHeight * index + _spaceBetweenLines * index;
            var height = lineHeight;
            var objRefSubRect = new Rect(rect.x, y, 140, height);
            var xButtonWidth = 35;
            var removeButtonRect = new Rect(rect.x + rect.width - xButtonWidth, y, xButtonWidth, height);
            var methodButtonRect = new Rect(rect.x + objRefSubRect.width, y, rect.width - objRefSubRect.width - xButtonWidth, height);

            var newTarget = EditorGUI.ObjectField(objRefSubRect, targets.GetArrayElementAtIndex(index).objectReferenceValue, typeof(Object), true);
            if (newTarget != targets.GetArrayElementAtIndex(index).objectReferenceValue)
            {
                targets.GetArrayElementAtIndex(index).objectReferenceValue = newTarget;
                methods.GetArrayElementAtIndex(index).stringValue = null;
                targets.serializedObject.ApplyModifiedProperties();
                methods.serializedObject.ApplyModifiedProperties();
            }

            if (newTarget is GameObject && GUI.Button(methodButtonRect, "Choose component..."))
            {
                var components = ((GameObject)newTarget).GetComponents<Component>().ToArray();
                var componentsNames = components.Select(c => c.GetType().Name).ToArray();

                var menu = new GenericMenu();
                for (int i = 0; i < componentsNames.Length; i++)
                {
                    menu.AddItem(new GUIContent(componentsNames[i]), targets.GetArrayElementAtIndex(index).objectReferenceValue == components[i], o =>
                    {
                        targets.GetArrayElementAtIndex(index).objectReferenceValue = (Object)o;
                        targets.serializedObject.ApplyModifiedProperties();
                    }, components[i]);
                }
                menu.ShowAsContext();
            }
            else if (newTarget is Component && GUI.Button(methodButtonRect, string.IsNullOrEmpty(methods.GetArrayElementAtIndex(index).stringValue) ? "Choose method..." : methods.GetArrayElementAtIndex(index).stringValue))
            {
                var validMethods = newTarget.GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance)
                    .Where(m => m.GetParameters().Count() == 1
                    && typeof(T).IsAssignableFrom(m.GetParameters().First().ParameterType));
                var methodNames = validMethods.Select(m => m.Name).ToArray();

                var menu = new GenericMenu();
                for (int i = 0; i < methodNames.Length; i++)
                {
                    menu.AddItem(new GUIContent(methodNames[i]), methods.GetArrayElementAtIndex(index).stringValue == methodNames[i], m =>
                    {
                        methods.GetArrayElementAtIndex(index).stringValue = (string)m;
                        methods.serializedObject.ApplyModifiedProperties();
                    }, methodNames[i]);
                }
                menu.ShowAsContext();
            }
            if (GUI.Button(removeButtonRect, "x"))
            {
                targets.GetArrayElementAtIndex(index).objectReferenceValue = null;
                methods.GetArrayElementAtIndex(index).stringValue = null;
                targets.DeleteArrayElementAtIndex(index);
                methods.DeleteArrayElementAtIndex(index);
                targets.serializedObject.ApplyModifiedProperties();
                methods.serializedObject.ApplyModifiedProperties();
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var multiplier = 2 + property.FindPropertyRelative("_permanentTargets").arraySize;

            return _lineHeight * multiplier + _spaceBetweenLines * (multiplier - 1) + _spaceBetweenProperties;
        }
    }
}