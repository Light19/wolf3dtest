﻿using UnityEditor;

namespace Wolf3DTest
{
    [CustomPropertyDrawer(typeof(SerializedScreenStateEvent))]
    public class SerializedScreenStateEventDrawer : SerializedGenericEventDrawer<IScreenState>
    {
    }
}