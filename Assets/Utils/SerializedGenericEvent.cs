﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Wolf3DTest
{
    [Serializable]
    public class SerializedGenericEvent<T>
    {
        [SerializeField]
        private List<Object> _permanentTargets = new List<Object>();
        [SerializeField]
        private List<string> _permanentMethods = new List<string>();
        [SerializeField]
        private event Action<T> _dynamicEvents;

        public void Invoke(T obj)
        {
            for (int i = 0; i < _permanentTargets.Count; i++)
            {
                var method = _permanentTargets[i].GetType().GetMethod(_permanentMethods[i]);
                method.Invoke(_permanentTargets[i], new object[] { obj });
            }
            _dynamicEvents?.Invoke(obj);
        }

        internal void AddListener(Action<T> listener)
        {
            _dynamicEvents += listener;
        }

        internal void RemoveListener(Action<T> listener)
        {
            _dynamicEvents -= listener;
        }
    }
}
