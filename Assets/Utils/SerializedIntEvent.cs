﻿using System;

namespace Wolf3DTest
{
    [Serializable]
    public class SerializedIntEvent : SerializedGenericEvent<int>
    {
    }
}
