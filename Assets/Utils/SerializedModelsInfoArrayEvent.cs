﻿using System;

namespace Wolf3DTest
{
    [Serializable]
    public class SerializedModelsInfoArrayEvent : SerializedGenericEvent<IModelsInfoProvider[]>
    {
    }
}
