﻿using System;

namespace Wolf3DTest
{
    [Serializable]
    public class SerializedModelsInfoProviderEvent : SerializedGenericEvent<IModelsInfoProvider>
    {
    }
}
