﻿using System.Runtime.CompilerServices;

namespace Wolf3DTest
{
    public interface IAwaiter : INotifyCompletion
    {
        bool IsCompleted { get; }
        void GetResult();
    }
}
