﻿using System;
using UnityEngine;

namespace Wolf3DTest
{
    [Serializable]
    public class SerializedQuaternionEvent : SerializedGenericEvent<Quaternion>
    {
    }
}
