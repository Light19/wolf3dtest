﻿using System;
using UnityEngine;

namespace Wolf3DTest
{
    [Serializable]
    public class SerializedVector3Event : SerializedGenericEvent<Vector3>
    {
    }
}
