﻿using System;
using System.Runtime.CompilerServices;

namespace Wolf3DTest
{
    public class LTDescrAwaiter : IAwaiter
    {
        public LTDescrAwaiter(LTDescr descr)
        {
            descr.setOnComplete(() => { _isCompleted = true; _continuation?.Invoke(); });
        }

        bool IAwaiter.IsCompleted => _isCompleted;
        private bool _isCompleted;

        void IAwaiter.GetResult()
        {

        }

        private Action _continuation;
        void INotifyCompletion.OnCompleted(Action continuation)
        {
            _continuation = continuation;
        }
    }
}
