﻿namespace Wolf3DTest
{
    public interface IAwaitable
    {
        IAwaiter GetAwaiter();
    }
}
