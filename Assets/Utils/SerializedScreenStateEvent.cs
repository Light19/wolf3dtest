﻿using System;

namespace Wolf3DTest
{
    [Serializable]
    public class SerializedScreenStateEvent : SerializedGenericEvent<IScreenState>
    {
    }
}
