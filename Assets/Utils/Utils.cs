﻿using System;
using System.IO;
using System.Security.Cryptography;
using UnityEngine;
using UnityExtension;

namespace Wolf3DTest
{
    public static class Utils
    {
        public static string GetHash(this byte[] bytes)
        {
            var result = string.Empty;
            using (SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider())
            {
                result = Convert.ToBase64String(sha1.ComputeHash(bytes));
            }
            return result;
        }

        public static void Populate(this Mesh mesh, byte[] objBytes)
        {
            var lStream = new MemoryStream(objBytes);
            var lOBJData = OBJLoader.LoadOBJ(lStream);
            mesh.LoadOBJ(lOBJData);
            lStream.Close();
        }

        public static IAwaitable GetAwaitable(this LTDescr descr)
        {
            return new LeanTweenTask(descr); 
        }
    }
}
