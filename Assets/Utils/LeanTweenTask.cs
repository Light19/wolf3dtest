﻿namespace Wolf3DTest
{
    public class LeanTweenTask : IAwaitable
    {
        private readonly LTDescr _descr;

        public LeanTweenTask(LTDescr descr)
        {
            _descr = descr;
        }

        IAwaiter IAwaitable.GetAwaiter()
        {
            return new LTDescrAwaiter(_descr);
        }
    }
}
