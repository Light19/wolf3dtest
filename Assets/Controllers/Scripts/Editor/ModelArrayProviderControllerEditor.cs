﻿using UnityEditor;

namespace Wolf3DTest
{
    [CustomEditor(typeof(ModelArrayProviderController))]
    public class ModelArrayProviderControllerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            EditorGUILayout.ObjectField(serializedObject.FindProperty("_modelsArrayProvider"), typeof(IModelsArrayProvider));
            serializedObject.ApplyModifiedProperties();

            EditorGUILayout.PropertyField(serializedObject.FindProperty("ModelUpdate"));
        }
    }
}
