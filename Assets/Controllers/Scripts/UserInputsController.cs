﻿using UnityEngine;

namespace Wolf3DTest
{
    public class UserInputsController : MonoBehaviour
    {
        public SerializedQuaternionEvent Rotate = new SerializedQuaternionEvent();
        public SerializedVector3Event Pan = new SerializedVector3Event();
        public SerializedVector3Event Scale = new SerializedVector3Event();


    }
}
