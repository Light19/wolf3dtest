﻿using UnityEngine;

namespace Wolf3DTest
{
    public class ModelArrayProviderController : MonoBehaviour
    {
        [SerializeField, HideInInspector]
        private ScriptableObject _modelsArrayProvider;

        public SerializedModelsInfoArrayEvent ModelUpdate = new SerializedModelsInfoArrayEvent();

        private void Start()
        {
            ModelUpdate.Invoke(((IModelsArrayProvider)_modelsArrayProvider).GetModelsInfo());
        }
    }
}
