﻿using System;
using UnityEngine;

namespace Wolf3DTest
{
    [CreateAssetMenu(fileName = nameof(PrefabModelArrayProvider), menuName = "Wolf3D/" + nameof(PrefabModelArrayProvider), order = 0)]
    public class PrefabModelArrayProvider : ModelArrayProviderBase, IModelsArrayProvider
    {
        [SerializeField]
        private TextAsset[] _objAssets;

        /// <summary>
        /// This is alike TestModelArrayProvider implementation - as simple, but can work in buil on Android
        /// </summary>
        /// <returns></returns>
        IModelsInfoProvider[] IModelsArrayProvider.GetModelsInfo()
        {
            if (ModelInfoProviderClassChoosed == null)
                throw new ApplicationException("ModelInfoProvider class isn't set");
            var result = new IModelsInfoProvider[_objAssets.Length];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = (IModelsInfoProvider)Activator.CreateInstance(ModelInfoProviderClassChoosed);
                var modelBytes = _objAssets[i].bytes;
                result[i].ObjBytes = modelBytes;
                result[i].Name = _objAssets[i].name;
            }
            return result;
        }
    }
}
