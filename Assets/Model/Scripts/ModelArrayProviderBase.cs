﻿using System;
using UnityEngine;

namespace Wolf3DTest
{
    //This class only handles appropriate class chosing
    public abstract class ModelArrayProviderBase : ScriptableObject
    {
        protected Type ModelInfoProviderClassChoosed => string.IsNullOrEmpty(_serializedChoosedModelInfoProviderType) ? null : Type.GetType(_serializedChoosedModelInfoProviderType, true);
        [SerializeField, HideInInspector]
        private string _serializedChoosedModelInfoProviderType;
    }
}
