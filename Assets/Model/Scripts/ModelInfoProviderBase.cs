﻿namespace Wolf3DTest
{
    /// <summary>
    /// This base class handles common functionality of computing obj's hash but doesn't handle previewImages and name fetching
    /// </summary>
    public abstract class ModelInfoProviderBase : IModelsInfoProvider
    {
        public virtual byte[] PreviewImageBytes { get; set; }
        public virtual string Name { get; set; }

        public byte[] ObjBytes
        {
            get
            {
                return _objBytes;
            }
            set
            {
                Hash = value.GetHash();
                _objBytes = value;
            }
        }
        private byte[] _objBytes;

        protected string Hash { get; private set; }
    }
}
