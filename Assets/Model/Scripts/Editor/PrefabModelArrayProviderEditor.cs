﻿using UnityEditor;

namespace Wolf3DTest
{
    [CustomEditor(typeof(PrefabModelArrayProvider))]
    public class PrefabModelArrayProviderEditor : ModelProviderBaseEditor
    {

    }
}