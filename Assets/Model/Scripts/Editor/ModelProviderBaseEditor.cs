﻿using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Wolf3DTest
{
    [CustomEditor(typeof(ModelArrayProviderBase))]
    public class ModelProviderBaseEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("ModelProviderType:");

            var chosenTypeProperty = serializedObject.FindProperty("_serializedChoosedModelInfoProviderType");
            var allAppropriateTypes = target.GetType().Assembly.GetTypes()
                .Where(t => typeof(IModelsInfoProvider).IsAssignableFrom(t))
                .Where(t => !typeof(IModelsInfoProvider).Equals(t))
                .Where(t => !t.IsAbstract).ToArray();
            if (GUILayout.Button(string.IsNullOrEmpty(chosenTypeProperty.stringValue)
                || allAppropriateTypes.Where(t => t.FullName == chosenTypeProperty.stringValue).Count() == 0 ? "Not chosen..." : chosenTypeProperty.stringValue))
            {
                var menu = new GenericMenu();
                for (int i = 0; i < allAppropriateTypes.Length; i++)
                {
                    menu.AddItem(new GUIContent(allAppropriateTypes[i].Name), allAppropriateTypes[i].FullName == chosenTypeProperty.stringValue, obj =>
                    {
                        chosenTypeProperty.stringValue = (string)obj;
                        serializedObject.ApplyModifiedProperties();
                    }, allAppropriateTypes[i].FullName);
                }
                menu.ShowAsContext();
            }
            EditorGUILayout.EndHorizontal();
             
        }
    }
}
