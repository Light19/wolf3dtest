﻿using System;
using System.IO;
using UnityEngine;

namespace Wolf3DTest
{
    [CreateAssetMenu(fileName = nameof(TestModelArrayProvider), menuName ="Wolf3D/" + nameof(TestModelArrayProvider), order = 0)]
    public class TestModelArrayProvider : ModelArrayProviderBase, IModelsArrayProvider
    {
        [SerializeField]
        private string[] _modelPaths;

        /// <summary>
        /// In this very basic implementation - we just loading predefined models from disk according to _modelPaths field
        /// </summary>
        /// <returns></returns>
        public IModelsInfoProvider[] GetModelsInfo()
        {
            if (ModelInfoProviderClassChoosed == null)
                throw new ApplicationException("ModelInfoProvider class isn't set");
            var result = new IModelsInfoProvider[_modelPaths.Length];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = (IModelsInfoProvider)Activator.CreateInstance(ModelInfoProviderClassChoosed);
                var modelBytes = File.ReadAllBytes(_modelPaths[i]);
                result[i].ObjBytes = modelBytes;
                result[i].Name = Path.GetFileName(_modelPaths[i]);
            }
            return result;
        }
    }
}
