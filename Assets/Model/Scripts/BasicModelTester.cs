﻿using UnityEngine;

namespace Wolf3DTest
{
    public class BasicModelTester : MonoBehaviour
    {
        public ScriptableObject ModelsArrayProvider;

        [ContextMenu("Test")]
        private void Test()
        {
            var modelsInfoProvidersArray = ((IModelsArrayProvider)ModelsArrayProvider).GetModelsInfo();

            for (int i = 0; i < modelsInfoProvidersArray.Length; i++)
            {
                var go = new GameObject($"Test_{i}");
                var meshFilter = go.AddComponent<MeshFilter>();
                var meshRenderer = go.AddComponent<MeshRenderer>();
                meshFilter.mesh.Populate(modelsInfoProvidersArray[i].ObjBytes);
            }
        }
    }
}
