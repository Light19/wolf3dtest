﻿namespace Wolf3DTest
{
    /// <summary>
    /// Handles fetching 3d-models array from hard disk/internet/playerprefs/etc
    /// </summary>
    public interface IModelsArrayProvider
    {
        IModelsInfoProvider[] GetModelsInfo();
    }
}

