﻿namespace Wolf3DTest
{
    /// <summary>
    /// Handles fetching 3d-model info
    /// </summary>
    public interface IModelsInfoProvider
    {
        byte[] ObjBytes { get; set; }
        byte[] PreviewImageBytes { get; set; }
        string Name { get; set; }
    }
}