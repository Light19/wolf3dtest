﻿using System;
using UnityEngine;

namespace Wolf3DTest
{
    //this guy is very simple - it stores prevew image in playerprefs, we shoul be cautious though - it has limit of 1 mb of information, so it's not very good way to store byte arrays
    public class TestModelInfoProvider : ModelInfoProviderBase
    {
        private const string _nameKey = "name";
        private const string _previewKey = "preview";

        public override byte[] PreviewImageBytes
        {
            get
            {
                if (!PlayerPrefs.HasKey($"{Hash}{_previewKey}"))
                    return null;
                var str = PlayerPrefs.GetString($"{Hash}{_previewKey}");
                return Convert.FromBase64String(str);
            }
            set
            {
                var val = Convert.ToBase64String(value);
                PlayerPrefs.SetString($"{Hash}{_previewKey}", val);
                PlayerPrefs.Save();
            }
        }

        public override string Name
        {
            get
            {
                if (!PlayerPrefs.HasKey($"{Hash}{_nameKey}"))
                    return null;
                return PlayerPrefs.GetString($"{Hash}{_nameKey}");
            }
            set
            {
                PlayerPrefs.SetString($"{Hash}{_nameKey}", value);
                PlayerPrefs.Save();
            }
        }
    }
}
