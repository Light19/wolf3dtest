﻿namespace Wolf3DTest
{
    public interface IScreenState
    {
        IAwaitable ExitState();
        IAwaitable EnterState();
    }
}