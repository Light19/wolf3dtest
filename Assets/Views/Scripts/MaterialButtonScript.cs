﻿using UnityEngine;

namespace Wolf3DTest
{
    public class MaterialButtonScript : MonoBehaviour
    {
        public int MaterialIndex;

        public SerializedIntEvent Click = new SerializedIntEvent();

        public void OnClick()
        {
            Click.Invoke(MaterialIndex);
        }
    }
}