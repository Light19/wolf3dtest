﻿using UnityEngine;

namespace Wolf3DTest
{
    public class ScreenCenterer : MonoBehaviour
    {
        private void Start()
        {
            var worldBounds = GetComponentInChildren<MeshRenderer>().bounds;
            var localBounds = GetComponentInChildren<MeshFilter>().mesh.bounds;
            var approxDistanceToCamera = worldBounds.size.z;

            var v3ViewPort = new Vector3(0, 0, approxDistanceToCamera);
            var v3BottomLeft = Camera.main.ViewportToWorldPoint(v3ViewPort);
            v3ViewPort.Set(1, 1, approxDistanceToCamera);
            var v3TopRight = Camera.main.ViewportToWorldPoint(v3ViewPort);
            v3ViewPort.Set(1, 1, approxDistanceToCamera);

            var targetSizeBounds = v3TopRight - v3BottomLeft; //Considering Y only as it's the smallest
            var targetScale = targetSizeBounds.y / worldBounds.size.y * 0.9f;

            var targetCenterPoint = v3BottomLeft + targetSizeBounds * 0.5f;
            transform.position = Camera.main.transform.position + Camera.main.transform.forward * worldBounds.size.z;

            var interpolator = gameObject.AddComponent<Interpolator>();
            interpolator.SetTargetLocalScale(Vector3.one * targetScale);
        }
    }
}
