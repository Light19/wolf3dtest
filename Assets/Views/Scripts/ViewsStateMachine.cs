﻿using System;
using System.Threading.Tasks;
using UnityEngine;

namespace Wolf3DTest
{
    public class ViewsStateMachine : MonoBehaviour
    {
        public enum Screens
        {
            SelectionScreen = 0,
            Model3DViewScreen
        }

        [SerializeField]
        private GameObject _modelSelectionView;
        [SerializeField]
        private GameObject _model3DVew;

        public async void SetState(int screen)
        {
            switch ((Screens)screen)
            {
                case Screens.SelectionScreen:
                    await deactivate(_model3DVew);
                    await activate(_modelSelectionView);
                    break;
                case Screens.Model3DViewScreen:
                    await deactivate(_modelSelectionView);
                    await activate(_model3DVew);
                    break;
            }
        }

        private async Task activate(GameObject go)
        {
            var screenState = go.GetComponentsInChildren<IScreenState>();
            for (int i = 0; i < screenState.Length; i++)
                await screenState[i].EnterState();
        }

        private async Task deactivate(GameObject go)
        {
            var screenState = go.GetComponentsInChildren<IScreenState>();
            for (int i = 0; i < screenState.Length; i++)
                await screenState[i].ExitState();
        }
    }
}
