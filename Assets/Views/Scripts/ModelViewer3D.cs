﻿using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Wolf3DTest
{
    public class ModelViewer3D : MonoBehaviour, IScreenState
    {
        private IModelsInfoProvider _currentModelsInfoProvider;
        public UnityEvent BackToMenu = new UnityEvent();
        private GameObject _3DModel;
        private GameObject _3DModelHolder;

        [SerializeField]
        private Material[] _materials;

        private int _activeMaterialIndex;
        private static readonly Vector2Int _previewResolution = Vector2Int.one * 256;

        private Sprite[] _materialScreenshootCaches;

        [SerializeField]
        private GameObject _materialButtonPrefab;
        private GameObject[] _materialButtons;

        private void cacheScreenshootsOfMaterialSpheres()
        {
            var sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.transform.position = Vector3.one * 99999;

            _materialScreenshootCaches = new Sprite[_materials.Length];
            for (int i = 0; i < _materials.Length; i++)
            {
                sphere.GetComponent<MeshRenderer>().material = _materials[i];
                var texture = takeScreenshoot(sphere, _previewResolution, 0.5f);
                var sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one * 0.5f, 100f);
                _materialScreenshootCaches[i] = sprite;
            }

            Destroy(sphere);
        }

        [SerializeField]
        private RectTransform _nonManipulatableArea;

        private void createMaterialButtons()
        {
            if (_materialScreenshootCaches == null)
                cacheScreenshootsOfMaterialSpheres();
            _materialButtons = new GameObject[_materials.Length];
            for (int i = 0; i < _materials.Length; i++)
            {
                _materialButtons[i] = Instantiate(_materialButtonPrefab, _materialButtonPrefab.transform.parent);
                _materialButtons[i].transform.SetSiblingIndex(_materialButtonPrefab.transform.GetSiblingIndex() + i + 1);
                _materialButtons[i].SetActive(true);
                _materialButtons[i].GetComponentInChildren<MaterialButtonScript>().MaterialIndex = i;
                _materialButtons[i].GetComponentInChildren<Image>().sprite = _materialScreenshootCaches[i];
            }
        }

        private void cleanSlate()
        {
            if (_materialButtons == null)
                return;
            for (int i = 0; i < _materialButtons.Length; i++)
            {
                Destroy(_materialButtons[i]);
            }
            _materialButtons = null;
        }

        public void RequireBackToMainMenuAction()
        {
            var screenShoot = takeScreenshoot(_3DModel, _previewResolution);
            _currentModelsInfoProvider.PreviewImageBytes = screenShoot.EncodeToPNG();
            BackToMenu.Invoke();
        }

        private Texture2D takeScreenshoot(GameObject model, Vector2Int previewResolution, float distanceCoeff = 1f)
        {
            var cameraGO = new GameObject("TempCamera");
            var camera = cameraGO.AddComponent<Camera>();
            var wolrdBounds = model.GetComponentInChildren<MeshRenderer>().bounds;
            cameraGO.transform.position = wolrdBounds.center + Vector3.one * wolrdBounds.size.magnitude * distanceCoeff;
            cameraGO.transform.LookAt(wolrdBounds.center);

            var rt = new RenderTexture(previewResolution.x, previewResolution.y, 24);
            camera.targetTexture = rt;
            var screenShot = new Texture2D(previewResolution.x, previewResolution.y, TextureFormat.RGB24, false);

            camera.Render();

            RenderTexture.active = rt;
            screenShot.ReadPixels(new Rect(0, 0, previewResolution.x, previewResolution.y), 0, 0);

            camera.targetTexture = null;
            RenderTexture.active = null;
            Destroy(rt);

            Destroy(cameraGO);

            screenShot.Apply();
            return screenShot;
        }

        public void OnModelActivates(IModelsInfoProvider modelInfoProvider)
        {
            if (_3DModel != null)
                DestroyModel();

            _currentModelsInfoProvider = modelInfoProvider;
            CreateModel(modelInfoProvider);
        }

        private void CreateModel(IModelsInfoProvider modelInfoProvider)
        {
            var go = new GameObject("Model");
            var meshFilter = go.AddComponent<MeshFilter>();
            var meshRenderer = go.AddComponent<MeshRenderer>();
            meshFilter.mesh.Populate(modelInfoProvider.ObjBytes);
            meshRenderer.material = _materials.Length > 0 ? _materials[_activeMaterialIndex] : null;

            var localBounds = meshFilter.mesh.bounds;
            _3DModelHolder = new GameObject("ModelHolder");

            go.transform.SetParent(_3DModelHolder.transform);
            go.transform.localPosition = Vector3.zero + localBounds.center * -1;
            go.transform.localRotation = Quaternion.identity;
            go.transform.localScale = Vector3.one;

            _3DModelHolder.transform.SetParent(transform);
            _3DModelHolder.transform.localPosition = Vector3.zero;
            _3DModelHolder.transform.localRotation = Quaternion.identity;
            _3DModelHolder.transform.localScale = Vector3.one;

            _3DModelHolder.AddComponent<ScreenCenterer>();
            var manipulatorScript = _3DModelHolder.AddComponent<Manipulator>();
            manipulatorScript.RestrictedArea = _nonManipulatableArea;
            _3DModel = go;

            fillStatistics();
        }

        private void fillStatistics()
        {
            var mesh = _3DModel.GetComponentInChildren<MeshFilter>().mesh;
            var sb = new StringBuilder();
            sb.AppendLine("Statistics");
            sb.AppendLine($"Vertices count: {mesh.vertices.Length}");
            sb.AppendLine($"Polygon count: {mesh.triangles.Length / 3}");
            sb.AppendLine($"Bounding box: ({mesh.bounds.size.x}, {mesh.bounds.size.y}, {mesh.bounds.size.z})");
            GetComponentsInChildren<Text>(true).Where(t => t.gameObject.name == "StatisticsText").First().text = sb.ToString();
        }

        private void DestroyModel()
        {
            Destroy(_3DModel);
        }

        public void SetRotation(Quaternion rotation)
        {
            _3DModel.transform.rotation = rotation;
        }

        public void SetPosition(Vector3 position)
        {
            _3DModel.transform.position = position;
        }

        public void SetScale(Vector3 scale)
        {
            _3DModel.transform.localScale = scale;
        }

        private float _rotationSpeedCoeff = 3f;

        IAwaitable IScreenState.ExitState()
        {
            var initialScale = _3DModel.transform.localScale;
            var xRotate = Random.Range(0f, 360f) * _rotationSpeedCoeff;
            var yRotate = Random.Range(0f, 360f) * _rotationSpeedCoeff;
            var zRotate = Random.Range(0f, 360f) * _rotationSpeedCoeff;
            return LeanTween.value(gameObject, 0f, 1f, 1f)
                .setEase(LeanTweenType.easeInOutCubic)
                .setOnUpdate((float val) =>
                {
                    _3DModelHolder.transform.Rotate(new Vector3(xRotate, yRotate, zRotate) * Time.deltaTime * val);
                    _3DModel.transform.localScale = Vector3.Lerp(initialScale, Vector3.one * 0.0001f, val);
                })
                .setOnComplete(() =>
                {
                    gameObject.SetActive(false);
                    cleanSlate();
                }).GetAwaitable();
        }

        IAwaitable IScreenState.EnterState()
        {
            createMaterialButtons();
            gameObject.SetActive(true);
            var xRotate = Random.Range(0f, 360f) * _rotationSpeedCoeff;
            var yRotate = Random.Range(0f, 360f) * _rotationSpeedCoeff;
            var zRotate = Random.Range(0f, 360f) * _rotationSpeedCoeff;
            return LeanTween.value(gameObject, 0f, 1f, 1f)
                .setEase(LeanTweenType.easeInOutCubic)
                .setOnUpdate((float val) =>
                {
                    _3DModelHolder.transform.rotation = Quaternion.Lerp(Quaternion.Euler(xRotate, yRotate, zRotate), Quaternion.identity, val);
                    _3DModel.transform.localScale = Vector3.Lerp(Vector3.one * 3f, Vector3.one, val);
                }).GetAwaitable();
        }

        public void SetMaterial(int i)
        {
            _activeMaterialIndex = i;
            _3DModel.GetComponent<MeshRenderer>().material = _materials.Length > i ? _materials[i] : null;
        }
    }
}
