﻿using UnityEngine;
using UnityEngine.UI;

namespace Wolf3DTest
{
    public class ModelGridElement : MonoBehaviour
    {
        public IModelsInfoProvider ModelInfoProvider;
        [SerializeField]
        private Image _sreenshootImage;

        public SerializedModelsInfoProviderEvent Clicked = new SerializedModelsInfoProviderEvent();

        public void OnClicked()
        {
            Clicked.Invoke(ModelInfoProvider);
        }

        public void SetImage(Sprite sprite)
        {
            _sreenshootImage.sprite = sprite;
        }

        public void SetName(string name)
        {
            GetComponentInChildren<Text>().text = name;
        }
    }
}
