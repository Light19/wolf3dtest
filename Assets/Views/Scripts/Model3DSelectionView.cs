﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Wolf3DTest
{
    public class Model3DSelectionView : MonoBehaviour, IScreenState
    {
        public SerializedModelsInfoProviderEvent ModelActivated = new SerializedModelsInfoProviderEvent();
        public UnityEvent To3DView = new UnityEvent();

        [SerializeField]
        private GameObject _gridElementPrefab;
        [SerializeField]
        private Sprite _noImageSprite;

        [SerializeField]
        private RectTransform _slidablePanel;

        private List<GameObject> _elements = new List<GameObject>();
        private IModelsInfoProvider[] _providers;

        public void RequireModelSelection(IModelsInfoProvider modelProvider)
        {
            ModelActivated.Invoke(modelProvider);
            To3DView.Invoke();
        }

        public void PopulateList(IModelsInfoProvider[] modelInfoProviders)
        {
            _providers = modelInfoProviders;
            cleanSlate();
            populate();
        }

        private void cleanSlate()
        {
            while (_elements.Count > 0)
            {
                Destroy(_elements[0]);
                _elements.RemoveAt(0);
            }
        }

        private void populate()
        {
            for (int i = 0; i < _providers.Length; i++)
            {
                var newElement = Instantiate(_gridElementPrefab, _gridElementPrefab.transform.parent);
                var elementScript = newElement.GetComponent<ModelGridElement>();
                elementScript.ModelInfoProvider = _providers[i];
                if (_providers[i].PreviewImageBytes != null)
                {
                    var texture = new Texture2D(1, 1);
                    texture.LoadImage(_providers[i].PreviewImageBytes);
                    var sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one * 0.5f, 100f);
                    elementScript.SetImage(sprite);
                }
                else
                    elementScript.SetImage(_noImageSprite);
                elementScript.SetName(_providers[i].Name);
                newElement.SetActive(true);
                _elements.Add(newElement);
            }
        }

        IAwaitable IScreenState.ExitState()
        {
            var rectTransform = _slidablePanel.GetComponent<RectTransform>();
            var initialPosition = rectTransform.anchoredPosition3D;
            var finalPosition = new Vector3(initialPosition.x + rectTransform.rect.x * 2, initialPosition.y, initialPosition.z);
            return LeanTween.value(gameObject, 0f, 1f, 1f)
                .setEase(LeanTweenType.easeInOutCubic)
                .setOnUpdate((float val) =>
                {
                    rectTransform.anchoredPosition3D = Vector3.Lerp(initialPosition, finalPosition, val);
                }).setOnComplete(()=>
                {
                    cleanSlate();
                    gameObject.SetActive(false);
                }).GetAwaitable();
        }

        IAwaitable IScreenState.EnterState()
        {
            populate();
            gameObject.SetActive(true);
            var rectTransform = _slidablePanel.GetComponent<RectTransform>();
            var initialPosition = rectTransform.anchoredPosition3D;
            var finalPosition = new Vector3(initialPosition.x - rectTransform.rect.x * 2, initialPosition.y, initialPosition.z);
            return LeanTween.value(gameObject, 0f, 1f, 1f)
                .setEase(LeanTweenType.easeInOutCubic)
                .setOnUpdate((float val) =>
                {
                    rectTransform.anchoredPosition3D = Vector3.Lerp(initialPosition, finalPosition, val);
                }).GetAwaitable();
        }
    }
}
