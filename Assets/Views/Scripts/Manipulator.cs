﻿using UnityEngine;

namespace Wolf3DTest
{
    public class Manipulator : MonoBehaviour
    {
        private const float _doubleTapInterval = 0.33f;

        private float _timeCounter;
        private int _tapCount;

        private Vector2? _lastTouchPosition;
        private float? _lastTouchesDistance;
        private Vector3? _initialScale;

        public RectTransform RestrictedArea;

        private void Update()
        {
            if (Input.touchCount == 0)
                return;

            var manipulatableScreenSpace = getScreenCoordinates(RestrictedArea);

            if (Input.touchCount == 1)
            {
                var touch = Input.GetTouch(0);

                if (RectTransformUtility.RectangleContainsScreenPoint(RestrictedArea, touch.position))
                    return;
                //if (!manipulatableScreenSpace.Contains(touch.position))
                //    return;

                switch (touch.phase)
                {
                    case TouchPhase.Moved:
                        if (_lastTouchPosition == null)
                        {
                            _lastTouchPosition = touch.position;
                            return;
                        }
                        //var worldBounds = GetComponent<MeshRenderer>().bounds;
                        //var touchedPos = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, (Camera.main.transform.position - worldBounds.center).magnitude));
                        transform.Rotate(new Vector3(touch.position.y - _lastTouchPosition.Value.y, _lastTouchPosition.Value.x - touch.position.x, 0), Space.World);
                        _lastTouchPosition = touch.position;
                        break;
                    case TouchPhase.Ended:
                    case TouchPhase.Canceled:
                        _lastTouchPosition = null;
                        break;
                }
            }
            else if (Input.touchCount == 2)
            {
                var touch1 = Input.GetTouch(0);
                var touch2 = Input.GetTouch(1);

                if (RectTransformUtility.RectangleContainsScreenPoint(RestrictedArea, touch1.position) ||
                    RectTransformUtility.RectangleContainsScreenPoint(RestrictedArea, touch2.position))
                    return;

                //if (!manipulatableScreenSpace.Contains(touch1.position) ||
                //    !manipulatableScreenSpace.Contains(touch2.position))
                //    return;

                if ((touch1.phase == TouchPhase.Moved || touch1.phase == TouchPhase.Began)
                    && (touch2.phase == TouchPhase.Moved || touch2.phase == TouchPhase.Began))
                {
                    if (_lastTouchesDistance == null)
                    {
                        _initialScale = transform.localScale;
                        _lastTouchesDistance = (touch1.position - touch2.position).magnitude;
                        return;
                    }

                    var targetScale = _lastTouchesDistance / (touch1.position - touch2.position).magnitude;
                    targetScale = (1f - targetScale) * 3f + 1f;
                    GetComponent<Interpolator>().SetTargetLocalScale(_initialScale.Value * targetScale.Value);
                    _lastTouchesDistance = (touch1.position - touch2.position).magnitude;
                }
                else
                {
                    _lastTouchesDistance = null;
                    _initialScale = null;
                }
            }
        }

        private Rect getScreenCoordinates(RectTransform uiElement)
        {
            var worldCorners = new Vector3[4];
            uiElement.GetWorldCorners(worldCorners);
            var rect = uiElement.rect;
            var result = new Rect(
                          worldCorners[0].x,
                          worldCorners[0].y,
                          worldCorners[2].x - worldCorners[0].x,
                          worldCorners[2].y - worldCorners[0].y);
            return result;
        }
    }
}
